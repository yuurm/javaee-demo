package com.topjava.javaeedemohello.entiny;

import java.util.Arrays;
import java.util.Optional;

public enum Role {
    USER,
    ADMIN;



//м-д нахождения роли
    public static Optional<Role> find(String role){
        return Arrays.stream(values()) //м-д возвращает список енумов
                .filter(role1 -> role1.name().equals(role))
                .findFirst();
    }
}
