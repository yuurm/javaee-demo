package com.topjava.javaeedemohello.entiny;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class Abonent extends Entity{
    private Integer id;
    private String name;
    private Integer phone;
}
