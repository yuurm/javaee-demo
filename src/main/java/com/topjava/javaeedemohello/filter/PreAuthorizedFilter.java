package com.topjava.javaeedemohello.filter;

import com.topjava.javaeedemohello.dto.UserDTO;
import com.topjava.javaeedemohello.util.URLHelper;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/admin")
public class PreAuthorizedFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        UserDTO user = (UserDTO) ((HttpServletRequest)request).getSession().getAttribute("user");
        if(user != null){
            chain.doFilter(request, response);
        } else {
            ((HttpServletResponse)response).sendRedirect(URLHelper.REGISTRATION);
        }
    }
}
