package com.topjava.javaeedemohello.filter;

import com.topjava.javaeedemohello.dto.UserDTO;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Set;


import static com.topjava.javaeedemohello.util.URLHelper.*;

@WebFilter("/*")
public class AuthorizedFilter implements Filter {

    private static final Set<String> ALLOWED_PATHS = Set.of(LOGIN, REGISTRATION, LOCALE);

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        String uriPath = ((HttpServletRequest)request).getRequestURI();
        if (isAllowedPath(uriPath)||isLoginSuccessed(request)){
    chain.doFilter(request, response);
        } else {
    deny(request, response);
        }
    }








    private void deny(ServletRequest request, ServletResponse response) throws IOException {
        String previousPage = ((HttpServletRequest)request).getHeader("referer");
        ((HttpServletResponse)response).sendRedirect(previousPage != null ? previousPage : LOGIN);
    }

    private boolean isLoginSuccessed(ServletRequest request) {
        UserDTO user = (UserDTO) ((HttpServletRequest)request).getSession().getAttribute("user");
        return user != null;
    }

    private boolean isAllowedPath(String uriPath) {
        return ALLOWED_PATHS.stream().anyMatch(uriPath::startsWith);
    }
}
