package com.topjava.javaeedemohello.exceptions;

import com.topjava.javaeedemohello.validator.ValidationError;

import java.util.List;

public class ValidationException extends RuntimeException {

    private final List<ValidationError> errorList;


    public ValidationException(List<ValidationError> errorList) {
        this.errorList = errorList;
    }

    public List<ValidationError> getErrorList() {
        return errorList;
    }
}
