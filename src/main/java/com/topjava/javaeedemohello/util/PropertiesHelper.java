package com.topjava.javaeedemohello.util;

import lombok.experimental.UtilityClass;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@UtilityClass
public class PropertiesHelper {
    private static final Properties properties = new Properties();

    static {
        connectionProperties();
    }

    private static void connectionProperties(){
        try(InputStream in = ConnectionCreator.class
                .getClassLoader()
                .getResourceAsStream("database.properties")){
            properties.load(in);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String get(String key){
        return properties.getProperty(key);
    }
}
