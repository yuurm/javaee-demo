package com.topjava.javaeedemohello.util;

import lombok.experimental.UtilityClass;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionCreator {

    private static final String user_db = "user";
    private static final String password_db = "password";
    private static final String url_db = "db.url";


    private ConnectionCreator() {
    }

    private static void registerDriver(){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    static {
        registerDriver();
    }

    public static Connection createConnection(){
        try {
            return DriverManager.getConnection(
                    PropertiesHelper.get(url_db),
                    PropertiesHelper.get(user_db),
                    PropertiesHelper.get(password_db)
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
