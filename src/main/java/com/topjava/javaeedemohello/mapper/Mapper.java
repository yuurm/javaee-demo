package com.topjava.javaeedemohello.mapper;

@FunctionalInterface
public interface Mapper <T, V>{

    V mapConvert(T object);
}
