package com.topjava.javaeedemohello.mapper;

import com.topjava.javaeedemohello.dto.GetUserDTO;
import com.topjava.javaeedemohello.entiny.Role;
import com.topjava.javaeedemohello.entiny.User;

public class GetUserMapper implements Mapper<GetUserDTO, User> {


    private static final GetUserMapper INSTANCE = new GetUserMapper();

    private GetUserMapper() {
    }


    public static GetUserMapper getInstance() {
        return INSTANCE;
    }

    @Override
    public User mapConvert(GetUserDTO object) {
        return User.builder()
                .name(object.getName())
                .email(object.getEmail())
                .password(object.getPassword()) ///************
                .role(Role.valueOf(object.getRole()))
                .build();
    }


}
