package com.topjava.javaeedemohello.mapper;

import com.topjava.javaeedemohello.dto.UserDTO;
import com.topjava.javaeedemohello.entiny.Role;
import com.topjava.javaeedemohello.entiny.User;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UserMapper implements Mapper<User, UserDTO> {

    private static final UserMapper INSTANCE = new UserMapper();

    @Override
    public UserDTO mapConvert(User object) {
        return UserDTO.builder()
                .id(object.getId())
                .name(object.getName())
                .email(object.getEmail())
                .role(object.getRole())
                .build();
    }

    public static UserMapper getInstance() {
        return INSTANCE;
    }
}
