package com.topjava.javaeedemohello.validator;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

public class ValidationResult {
    @Getter
    private final List<ValidationError> errorList = new ArrayList<>();

    public void add(ValidationError error) {
        errorList.add(error);
    }

    public boolean isValid() {
        return errorList.isEmpty();
    }
}
