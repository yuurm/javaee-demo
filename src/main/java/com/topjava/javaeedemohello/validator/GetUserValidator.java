package com.topjava.javaeedemohello.validator;

import com.topjava.javaeedemohello.dto.GetUserDTO;
import com.topjava.javaeedemohello.entiny.Role;

//валидация данных при создании пользователя
public class GetUserValidator implements Validator<GetUserDTO> {

    private static final GetUserValidator INSTANCE = new GetUserValidator();


    public static GetUserValidator getInstance() {
        return INSTANCE;
    }

    @Override
    public ValidationResult isValid(GetUserDTO object) {

        ValidationResult validationResult = new ValidationResult();


        if (Role.find(String.valueOf(object.getRole())).isEmpty()) {
            validationResult.add(ValidationError.of("invalid.role", "INVALID ROLE!!!"));
        }
        return validationResult;
    }
}
