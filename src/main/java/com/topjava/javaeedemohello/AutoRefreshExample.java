package com.topjava.javaeedemohello;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalTime;



@WebServlet("/auto-refresh-example")
public class AutoRefreshExample extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setIntHeader("Refresh", 1);

        resp.setContentType("text/html");

        LocalTime currentTime = LocalTime.now();

        PrintWriter out = resp.getWriter();
        String title = "DateTime example";
        String docType = "<!DOCTYPE html>";



        out.println(docType + "<html><head>"+
                "<title>" + title + "</title>" +
                "</head>"+
                "<body>" + currentTime.toString() + "</dody></html>" );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
