package com.topjava.javaeedemohello;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

import static java.time.LocalTime.now;

@WebServlet("/date-time-example")
public class DateTimeExample extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        String title = "DateTime example";
        String docType = "<!DOCTYPE html>";



        out.println(docType + "<html><head>"+
                "<title>" + title + "</title>" +
                "</head>"+
                "<body>" + now().toString() + "</dody></html>" );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
