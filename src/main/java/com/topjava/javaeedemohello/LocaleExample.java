package com.topjava.javaeedemohello;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalTime;
import java.util.Locale;


@WebServlet("/locale-example")
public class LocaleExample extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Locale locale = new Locale("ru_RU");

        String lang = locale.getLanguage();

        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");


        PrintWriter out = resp.getWriter();
        String title = "Locale example";
        String docType = "<!DOCTYPE html>";


        if (locale.getLanguage().equalsIgnoreCase("ru_ru")) {
            title = "Демо локализации";
            out.println(docType + "<html><head>" +
                    "<title>" + title + "</title>" +
                    "</head>" +
                    "<body>" +
                    "<h1>Локализация: </h1>" + locale +
                    "<h2>Язык: </h2>" + lang + "</dody></html>");
        } else {
            out.println(docType + "<html><head>" +
                    "<title>" + title + "</title>" +
                    "</head>" +
                    "<body>" +
                    "<h1>Locale data: </h1>" + locale +
                    "<h2>Language: </h2>" + lang + "</dody></html>");
        }
    }




    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
