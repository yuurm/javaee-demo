package com.topjava.javaeedemohello;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.print.Printable;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;

@WebServlet("/custom-request-example")
public class ServletRequestExample extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        String title = "Custom HTTP Request";

        String contentType = "<!DOCTYPE html>\n";
        out.println(contentType +
                "<html>\n" +
                "<head><title>" + title + "</title></head>"+
                "<body>" +
                "<h1>HTTP Request Servlet</h1>");

        Enumeration headers = req.getHeaderNames();

        while(headers.hasMoreElements()){
            String parameterName = (String) headers.nextElement();
            String parameterValue = req.getHeader(parameterName);
            out.println("<h3>" + parameterName + ": " + parameterValue + " </h3>\n</body>");
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
