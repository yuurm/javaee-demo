package com.topjava.javaeedemohello;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
@WebServlet("/session-example")
public class SessionExample extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        String message = "";
        String sessionId = session.getId();
        Date sessionCreationDate = new Date(session.getCreationTime());
        Date lastSessionAccess = new Date(session.getLastAccessedTime());
        String userId = "777";

        if(session.isNew()){
            message = "Hello, user!";
        } else {
            message = "Hello again, user!";
        }

        resp.setContentType("text/html");

        PrintWriter out = resp.getWriter();
        String title = "Session example";
        String docType = "<!DOCTYPE html>";
        out.println(docType + "<head>"+
                "<title>" + title + "</title>" +
                "</head>"+
                "<body>" +
                "<h1>Session info: </h1>" +
                        "<h2>" + message + "</h2>"+
                "Session id: " + sessionId +
                "<br/>" +
                "Session created: " + sessionCreationDate +
                "<br/>" +
                "Last Session Access: " + lastSessionAccess +
                "<br/>" +
                "User Id: " + userId +
                "</body>" +
                        "</html>"
                );
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
