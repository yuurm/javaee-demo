package com.topjava.javaeedemohello.service;

import com.topjava.javaeedemohello.dao.impl.AbonentDaoImpl;

import com.topjava.javaeedemohello.dto.AbonentDTO;

import com.topjava.javaeedemohello.exceptions.DaoException;


import java.util.List;
import java.util.stream.Collectors;


public class AbonentService {
    private static final AbonentService INSTANCE = new AbonentService();

    private final AbonentDaoImpl abonentDaoImp = AbonentDaoImpl.getInstance();

    private AbonentService() {
    }


    public List<AbonentDTO> findAll() throws DaoException {
        return abonentDaoImp.findAll().stream()
                .map(abonent -> AbonentDTO.builder()
                        .id(abonent.getId())
                        .name(abonent.getName())
                        .phone(abonent.getPhone())
                        .build())
                .collect(Collectors.toList());
    }


    public static AbonentService getInstance() {
        return INSTANCE;
    }
}
