package com.topjava.javaeedemohello.service;

import com.topjava.javaeedemohello.dao.UserDao;
import com.topjava.javaeedemohello.dao.impl.UserDaoImpl;
import com.topjava.javaeedemohello.dto.GetUserDTO;
import com.topjava.javaeedemohello.dto.UserDTO;
import com.topjava.javaeedemohello.entiny.User;
import com.topjava.javaeedemohello.exceptions.DaoException;
import com.topjava.javaeedemohello.exceptions.ValidationException;
import com.topjava.javaeedemohello.mapper.GetUserMapper;
import com.topjava.javaeedemohello.mapper.UserMapper;
import com.topjava.javaeedemohello.validator.GetUserValidator;
import com.topjava.javaeedemohello.validator.ValidationError;
import com.topjava.javaeedemohello.validator.ValidationResult;
import lombok.SneakyThrows;

import java.util.Optional;


public class UserService {
    private static final UserService INSTANCE = new UserService();
    private final GetUserValidator userValidator = GetUserValidator.getInstance();
    private final UserDaoImpl userDaoImp = UserDaoImpl.getInstance();

    private final GetUserMapper getUserMapper = GetUserMapper.getInstance();
    private final UserMapper userMapper = UserMapper.getInstance();


    //вход в приложение

    public Optional<UserDTO> login(String email, String password) throws DaoException {
        return userDaoImp
                .findUserByEmailAndPassword(email, password)
                .map(userMapper::mapConvert);
    }


    //регистрация
    public Integer createUser(GetUserDTO userDTO) throws DaoException {
        ValidationResult validationResult = userValidator.isValid(userDTO);
        if(!validationResult.isValid()){
            throw new ValidationException(validationResult.getErrorList());
        }

        User user = getUserMapper.mapConvert(userDTO);

        userDaoImp.save(user);

        return user.getId();
    }


    public static UserService getInstance(){
        return INSTANCE;
    }
}
