package com.topjava.javaeedemohello.dto;


import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class AbonentDTO {
    Integer id;
    String name;
    Integer phone;
}
