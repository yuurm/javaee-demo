package com.topjava.javaeedemohello.dto;

import com.topjava.javaeedemohello.entiny.Role;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class GetUserDTO {
    String name;
    String email;
    String password;
    String role;
}
