package com.topjava.javaeedemohello;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

@WebServlet("/cookie-example")
public class CookieExample extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie sessionId = new Cookie("session_Id", req.getRemoteAddr() + "777");
        Cookie language = new Cookie("language", req.getLocale().toString());

        resp.addCookie(sessionId);
        resp.addCookie(language);

        resp.setContentType("text/html");

        PrintWriter out = resp.getWriter();
        String title = "Cookies example";
        String docType = "<!DOCTYPE html>";

        Cookie[] cookies = req.getCookies();

        out.println(docType + "<html><head>"+
                "<title>" + title + "</title>" +
                "</head>"+
                "<body>");

        if(cookies != null){
            out.println("Cookies: ");
            for (int i = 0; i < cookies.length; i++){
                out.println("<hr/>");
                out.println("Cookie " + i + " : " + cookies[i].getName());
                out.println("<br/>");
                out.println("Cookie " + i + " : " + cookies[i].getValue());
                out.println("<hr/>");
            }
        } else {
            out.println("Not cookies yet!!!");
        }
        out.println("</body></html>");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
