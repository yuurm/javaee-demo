package com.topjava.javaeedemohello;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

@WebServlet("/db-example")
public class DBExample extends HttpServlet {

    static final String JDBC_DRIVER = "org.postgresql.Driver";
    static final String DB_URL = "jdbc:postgresql://localhost:5432/postgres";
    static final String DB_USER = "postgres";
    static final String DB_PASSWORD = "postgres";
    static final String SELECT_ALL_EMPLOYEES = "SELECT * FROM employees";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");

        PrintWriter out = resp.getWriter();
        String title = "DB example";
        String docType = "<!DOCTYPE html>";

        try {
            Class.forName(JDBC_DRIVER);
            Connection connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASSWORD);

            Statement statement = connection.createStatement();

            ResultSet resultSet = statement.executeQuery(SELECT_ALL_EMPLOYEES);

            out.println(docType + "<html><head>" +
                    "<title>" + title + "</title>" +
                    "</head>" +
                    "<body>");
            out.println("<h1>Employees: </h1>");
            out.println("<br/>");
            while(resultSet.next()){
                int id = resultSet.getInt(1);
                String firstName = resultSet.getString(2);
                String lastName = resultSet.getString(3);
                String profession = resultSet.getString(4);
                int age = resultSet.getInt(5);
                double salary = resultSet.getDouble(6);

                out.println("Id: " + id);
                out.println("<br/>");
                out.println("First Name: " + firstName);
                out.println("<br/>");
                out.println("Last Name: " + lastName);
                out.println("<br/>");
                out.println("Profession: " + profession);
                out.println("<br/>");
                out.println("Age: " + age);
                out.println("<br/>");
                out.println("Salary: " + salary);
                out.println("<br/>");


            }

            resultSet.close();
            statement.close();
            connection.close();


        } catch (ClassNotFoundException | SQLException e) {
            throw new RuntimeException(e);
        }





    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
