package com.topjava.javaeedemohello;

import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet("/hello-servlet")
public class HelloServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();

        String name = req.getParameter("studentname");
        String gender = req.getParameter("gender");
        String group = req.getParameter("group");
        String[] lessons = req.getParameterValues("lessons");

        try{
        out.println("<p>Name: " + name + "</p>");
        out.println("<p>Gender: " + gender + "</p>");
        out.println("<p>Group: " + group + "</p>");
        out.println("<h3>lessons: </h3>");
        for(String lesson: lessons)
            out.println("<li>" + lesson + "</li>");
    } finally {
            out.close();
        }
        }

    /*
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        String name = request.getParameter("name");

        out.println("<html><body>");
        out.println("<h1>" + name + "</h1>");
        out.println("</body></html>");
    }

     */


}