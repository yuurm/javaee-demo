package com.topjava.javaeedemohello.dao;

import com.topjava.javaeedemohello.entiny.User;
import com.topjava.javaeedemohello.exceptions.DaoException;


import java.util.List;
import java.util.Optional;

public interface UserDao <Integer, User>{

    List<User> findAll() throws DaoException;


    Optional<User> findById(Integer id) throws DaoException;


    User save(User entity) throws DaoException;


    void update(User entity) throws DaoException;


    boolean delete(Integer id) throws DaoException;
}
