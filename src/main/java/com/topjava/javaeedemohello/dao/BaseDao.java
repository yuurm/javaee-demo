package com.topjava.javaeedemohello.dao;



import com.topjava.javaeedemohello.entiny.Entity;
import com.topjava.javaeedemohello.exceptions.DaoException;

import java.util.List;
import java.util.Optional;

public interface BaseDao<K, T extends Entity> {
    public List<T> findAll() throws DaoException;

    public Optional<T>findById(K id) throws DaoException;

    public void update(T entity) throws DaoException;

    public T save(T entity) throws DaoException;

    public boolean delete(K id) throws DaoException;


}
