package com.topjava.javaeedemohello.dao.impl;


import com.topjava.javaeedemohello.dao.UserDao;
import com.topjava.javaeedemohello.entiny.Role;
import com.topjava.javaeedemohello.entiny.User;
import com.topjava.javaeedemohello.exceptions.DaoException;
import com.topjava.javaeedemohello.util.ConnectionCreator;
import lombok.SneakyThrows;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class UserDaoImpl implements UserDao<Integer, User> {
    private static final UserDaoImpl INSTANCE = new UserDaoImpl();

    private static final String SAVE_SQL = "INSERT INTO users (name, email, password, role) " +
            "VALUES (?, ?, ?, ?)";


    private static final String FIND_BY_EMAIL_AND_PASSWORD = "SELECT " +
            "* FROM users " +
            "WHERE email = ? AND password = ?";
//******
    private UserDaoImpl() {
    }


    @Override
    public List<User> findAll() throws DaoException {
        return null;
    }

    @Override
    public Optional<User> findById(Integer id) throws DaoException {
        return Optional.empty();
    }


    //JDBC реализация для регистрации
    @SneakyThrows
    @Override
    public User save(User entity) throws DaoException {
        try (Connection connection = ConnectionCreator.createConnection();
             //из Java в BD
             PreparedStatement preparedStatement = connection.prepareStatement(SAVE_SQL, RETURN_GENERATED_KEYS)) {
            preparedStatement.setObject(1, entity.getName());
            preparedStatement.setObject(2, entity.getEmail());
            preparedStatement.setObject(3, entity.getPassword());
            preparedStatement.setObject(4, entity.getRole().name());
            preparedStatement.executeUpdate();
            //из BD в Java
            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            //if (generatedKeys.next()) {
            generatedKeys.next();
            entity.setId(generatedKeys.getObject("id", Integer.class));
            //}
            return entity;
        }
        /*
        catch (SQLException e) {
            throw new RuntimeException(e);
        }

         */
    }

    @Override
    public void update(User entity) throws DaoException {

    }

    @Override
    public boolean delete(Integer id) throws DaoException {
        return false;
    }

    //JDBC реализация для login


    @SneakyThrows
    public Optional<User> findUserByEmailAndPassword(String email, String password) throws DaoException {
        try (Connection connection = ConnectionCreator.createConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_EMAIL_AND_PASSWORD)) {
            preparedStatement.setString(1, email);
            preparedStatement.setString(2, password);


            ResultSet resultSet = preparedStatement.executeQuery();
            User user = null;
            if (resultSet.next()) {
                user = buildUser(resultSet);
            }
            return Optional.ofNullable(user);
        }
    }


    public static UserDaoImpl getInstance() {
        return INSTANCE;
    }

    private User buildUser(ResultSet resultSet) throws SQLException {
        //lombok builder
        return User.builder()
                .id(resultSet.getObject("id", Integer.class))
                .name(resultSet.getObject("name", String.class))
                .email(resultSet.getObject("email", String.class))
                .password(resultSet.getObject("password", String.class))
               // .role(Role.find(resultSet.getObject("role", String.class)).orElse(null))
                .role(Role.valueOf(resultSet.getObject("role", String.class)))
                .build();

    }


}
