package com.topjava.javaeedemohello.dao.impl;


import com.topjava.javaeedemohello.dao.AbonentDao;
import com.topjava.javaeedemohello.entiny.Abonent;
import com.topjava.javaeedemohello.exceptions.DaoException;
import com.topjava.javaeedemohello.util.ConnectionCreator;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class AbonentDaoImpl implements AbonentDao <Integer, Abonent> {
    private static final AbonentDaoImpl INSTANCE = new AbonentDaoImpl();

    private static final String DELETE_SQL = "DELETE FROM phonebook WHERE idphonebook = ?";

    private static final String SAVE_SQL = "INSERT INTO phonebook (lastname, phone) " +
            "VALUES (?, ?)";

    private static final String UPDATE_SQL = "UPDATE phonebook " +
            "SET idphonebook = ?, " +
            "lastname = ?, " +
            "phone= ?" +
            "WHERE idphonebook = ?";

    private static final String FIND_ALL_SQL = "SELECT idphonebook, lastname, phone FROM phonebook";

    private static final String FIND_BY_ID_SQL = "SELECT idphonebook, lastname, phone FROM phonebook WHERE idphonebook = ?";

    private AbonentDaoImpl() {
    }


    @Override
    public List<Abonent> findAll() throws DaoException {
        try(Connection connection = ConnectionCreator.createConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_ALL_SQL)){
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Abonent>abonents = new ArrayList<>();
            while (resultSet.next()){
                abonents.add(buildAbonent(resultSet));
            }
            return abonents;
        } catch (SQLException e) {
            throw new DaoException(e);
        }

    }

    @Override
    public Optional<Abonent> findById(Integer id) throws DaoException {
        try(Connection connection = ConnectionCreator.createConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(FIND_BY_ID_SQL)){
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            Abonent abonent = null;
            if (resultSet.next()){
                abonent = buildAbonent(resultSet);
            }
            return Optional.ofNullable(abonent);
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public Abonent save(Abonent abonent) throws DaoException {
        try(Connection connection = ConnectionCreator.createConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(SAVE_SQL, Statement.RETURN_GENERATED_KEYS)){
            //preparedStatement.setInt(1, abonent.getId());
            preparedStatement.setString(1, abonent.getName());
            preparedStatement.setInt(2, abonent.getPhone());
            preparedStatement.executeUpdate();

            ResultSet generatedKeys = preparedStatement.getGeneratedKeys();
            if(generatedKeys.next()){
                abonent.setId(generatedKeys.getInt("idphonebook"));
            }
            return abonent;
        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public void update(Abonent abonent) throws DaoException {
        try(Connection connection = ConnectionCreator.createConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_SQL)){
            preparedStatement.setInt(1, abonent.getId());
            preparedStatement.setString(2, abonent.getName());
            preparedStatement.setInt(3, abonent.getPhone());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    @Override
    public boolean delete(Integer id) throws DaoException {
        try(Connection connection = ConnectionCreator.createConnection();
            PreparedStatement preparedStatement = connection.prepareStatement(DELETE_SQL)){
            preparedStatement.setInt(1, id);
           return preparedStatement.executeUpdate() > 0;

        } catch (SQLException e) {
            throw new DaoException(e);
        }
    }

    public static AbonentDaoImpl getInstance(){
        return INSTANCE;
    }

    private Abonent buildAbonent(ResultSet resultSet) throws SQLException {
        return new Abonent(
                resultSet.getObject("idphonebook", Integer.class),
                resultSet.getObject("lastname", String.class),
                resultSet.getObject("phone", Integer.class)
        );
    }
}
