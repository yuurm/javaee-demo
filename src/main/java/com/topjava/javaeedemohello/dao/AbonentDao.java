package com.topjava.javaeedemohello.dao;



import com.topjava.javaeedemohello.entiny.Abonent;
import com.topjava.javaeedemohello.entiny.Entity;
import com.topjava.javaeedemohello.exceptions.DaoException;

import java.util.List;
import java.util.Optional;

public interface AbonentDao<I extends Number, A extends Entity> extends BaseDao<Integer, Abonent>{

    List<Abonent> findAll() throws DaoException;


    Optional<Abonent> findById(Integer id) throws DaoException;


    Abonent save(Abonent entity) throws DaoException;


    void update(Abonent entity) throws DaoException;


    boolean delete(Integer id) throws DaoException;
}
