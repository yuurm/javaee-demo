package com.topjava.javaeedemohello.servlets;

import com.topjava.javaeedemohello.dto.GetUserDTO;
import com.topjava.javaeedemohello.entiny.Role;
import com.topjava.javaeedemohello.exceptions.ValidationException;
import com.topjava.javaeedemohello.service.UserService;
import com.topjava.javaeedemohello.util.URLHelper;
import lombok.SneakyThrows;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(value = URLHelper.REGISTRATION, name = "RegistrationServlet")
public class RegistrationServlet extends HttpServlet {
    private final UserService userService = UserService.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("roles", Role.values());

        req.getRequestDispatcher("WEB-INF/jsp/registration.jsp").forward(req, resp);
    }


    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        GetUserDTO userDTO = GetUserDTO.builder()
                .name(req.getParameter("name"))
                .email(req.getParameter("email"))
                .password(req.getParameter("password"))
                .role(req.getParameter("role"))
                .build();

        try {
            userService.createUser(userDTO);
            resp.sendRedirect(URLHelper.LOGIN);
        } catch (ValidationException e) {
            req.setAttribute("errors", e.getErrorList());
            doGet(req, resp);
        }
    }
}
