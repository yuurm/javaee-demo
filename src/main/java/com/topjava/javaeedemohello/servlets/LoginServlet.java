package com.topjava.javaeedemohello.servlets;

import com.topjava.javaeedemohello.dto.GetUserDTO;
import com.topjava.javaeedemohello.dto.UserDTO;
import com.topjava.javaeedemohello.entiny.Role;
import com.topjava.javaeedemohello.exceptions.ValidationException;
import com.topjava.javaeedemohello.service.UserService;
import com.topjava.javaeedemohello.util.URLHelper;
import lombok.SneakyThrows;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(URLHelper.LOGIN)
public class LoginServlet extends HttpServlet {
    private final UserService userService = UserService.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("WEB-INF/jsp/login.jsp").forward(req, resp);
    }


    @SneakyThrows
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        userService.login(req.getParameter("email"), req.getParameter("password"))
                .ifPresentOrElse(
                        userDTO -> {
                            try {
                                onSuccessLogin(userDTO, req, resp);
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        },
                        () -> {
                            try {
                                onFailedLogin(req, resp);
                            } catch (IOException e) {
                                throw new RuntimeException(e);
                            }
                        });


    }

    //***************
    private void onFailedLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(URLHelper.LOGIN + "?error&email=" + req.getParameter("email"));
    }

    private void onSuccessLogin(UserDTO userDTO, HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().setAttribute("user", userDTO);
        resp.sendRedirect(URLHelper.ABONENTS);
    }
}
