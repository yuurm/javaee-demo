package com.topjava.javaeedemohello.servlets;

import com.topjava.javaeedemohello.util.URLHelper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(URLHelper.LOCALE)
public class LocaleServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//*****
        String language = req.getParameter("lang");
        req.getSession().setAttribute("lang", language);


        String previousPage = req.getHeader("referer");
        String currentPage = previousPage != null ? previousPage : URLHelper.LOGIN;
        resp.sendRedirect(currentPage + "?lang=" + language);
    }
}
