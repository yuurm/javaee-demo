package com.topjava.javaeedemohello.servlets;

import com.topjava.javaeedemohello.dto.UserDTO;
import com.topjava.javaeedemohello.service.AbonentService;
import com.topjava.javaeedemohello.service.UserService;
import com.topjava.javaeedemohello.util.URLHelper;
import lombok.SneakyThrows;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/abonents")
public class AbonentsServlet extends HttpServlet {
    private final AbonentService abonentService = AbonentService.getInstance();

    @SneakyThrows
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setAttribute("abonents", abonentService.findAll());
        req.getRequestDispatcher("WEB-INF/jsp/abonents.jsp").forward(req, resp);
    }





}
