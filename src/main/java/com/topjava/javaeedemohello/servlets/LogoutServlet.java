package com.topjava.javaeedemohello.servlets;

import com.topjava.javaeedemohello.dto.UserDTO;
import com.topjava.javaeedemohello.service.UserService;
import com.topjava.javaeedemohello.util.URLHelper;
import lombok.SneakyThrows;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(URLHelper.LOGOUT)
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getSession().invalidate();
        resp.sendRedirect(URLHelper.LOGIN);
    }
}
