<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<%@include file="header.jsp"%>

<form action="${pageContext.request.contextPath}/registration" method="post" >
    <label for="name">Name:
        <input type="text" name="name" id="name">
    </label><br>
    <label for="emailId">Email:
        <input type="text" name="email" id="emailId">
    </label><br>
    <label for="passwordId">Password:
        <input type="text" name="password" id="passwordId">
    </label><br>
    <label for="role">Role:
        <select name="role" id="role">
        <c:forEach var="role" items="${requestScope.roles}">
            <option value="${role}">${role}</option>
        </c:forEach>
        </select>
    </label>
    <br>
    <button type="submit">Submit</button>
    <c:if test="{not empty requestScope.errors}">
    <div style="color: darkmagenta">
    <c:forEach var="error" items="${requestScope.errors}">
        <span>${error.message}</span>
        <br>
    </c:forEach>
</div>
</c:if>
</form>
</body>
</html>
