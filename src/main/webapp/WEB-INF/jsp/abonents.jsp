<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%@ include file="header.jsp"%>

<c:if test="${not empty requestScope.abonents}">

<h1>List of abonents:</h1>
<ul>

    <c:forEach var="abonent" items="${requestScope.abonents}">
        <li>
            ${abonent.name}
        </li>
    </c:forEach>
</ul>
</c:if>


</body>
</html>
