<%@ page import="java.time.LocalDate" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%
    String[]employees = new String[]{"Ivan", "Petr", "Mariya"};
%>
<%--Это комментарий--%>
<%!
    String sayHi (){
        return "Say hi!";
    }
%>
<!DOCTYPE html>
<html>
<head>
    <title>JSP - Hello World</title>
</head>
<body>
<jsp:include page="header.html"/>
<h1><%= "Hello World!" %>
</h1>
<br/>
<p>Current date: <%=LocalDate.now()%></p>
<p>Hello String: <%= new String("Hello Topjava").toLowerCase()%></p>
<a href="hello-servlet">Hello Servlet</a>
<%
   for(int i = 1; i < 10; i++){
       out.println("<br>Counter " + i);
   }
%>
<ul>
    <%
        for(String employee : employees){
            out.println("<li>" + sayHi() + " " + employee + "</li>");
        }

%>
</ul>
<jsp:include page="WEB-INF/jsp/footer.jsp"/>
</body>
</html>